/* 
 * File:   main.cpp
 * @author Stijn Manhaeve, Ben Cardoen.
 *
 * Created on 28 March 2016, 16:57
 */

#include <cstdlib>
#include <atomic>
#include <iostream>
#include <thread>
#include <vector>
#include <bits/std_mutex.h>
#include <mutex>
#include <cassert>


/**
 * This is democode of transactional memory support in G++. Exceptions are not rolled back, and we still use
 * the old G++ syntax since that allows us to demonstrate cancel.
 */

struct Balance{
    typedef double value_type;
    value_type cbal;
    std::mutex lock;
    Balance(value_type d):cbal(d){;}
    // Cannot define operator +=, assignment, copyconstructor with mutex, at least not trivially
};

/**
 * Simulate a call that unexpected breaks you transaction.
 * This is still a safe function, it needs no marking since the compiler can see that
 *  - it contains no accesses to volatile members/vars
 *  - it calls no unsafe functions
 *  - it has no atomic blocks.
 * If you call rand(), then the compiler will stop with an error, since rand() is not t-safe.
 * This function, if called in an atomic block, will also be rolled back, this is in effect a noop.
 * @return 
 */
bool unsafe(){
    thread_local int i = 0;
    if(i++ % 2)
        return false;
    return true;
}

// Basic sig.
void transfer(Balance& from, Balance& to, Balance::value_type amt);

// No locking, no atomicity
void transfer_naive(Balance& from, Balance& to, Balance::value_type amt){
    from.cbal -= amt;
    if(unsafe())return;
    to.cbal += amt;
}

// std::atomic is not an option, it's a 1-member lock

// Locked, but not exception safe.
void transfer_locked(Balance& from, Balance& to, Balance::value_type amt){
    from.lock.lock();
    to.lock.lock();
    Balance::value_type fromold = from.cbal;
    Balance::value_type   toold = to.cbal; 
    fromold -= amt;         
    bool b = unsafe(); // Exception here, and that's all folks.
    toold += amt;           
    if(!b){
        from.cbal = fromold;
        // What if this crashes
        to.cbal = toold;
    }
    to.lock.unlock();
    from.lock.unlock();
}

// Locked, exception safe, but what about rollbacks?
void transfer_locked_esafe(Balance& from, Balance& to, Balance::value_type amt){
    std::lock(from.lock, to.lock);  // Ordered, exception safe.
    Balance::value_type fromold = from.cbal;
    Balance::value_type   toold = to.cbal; 
    fromold -= amt;
    bool b = unsafe();
    toold += amt;
    if(!b){
        from.cbal = fromold;
        // No rollback.
        to.cbal = toold;
    }
}

bool transfer_locked_esafe_cas_fail(Balance& from, Balance& to, Balance::value_type amt){
    bool success = false;
    std::lock(from.lock, to.lock);  // Ordered, exception safe.
    Balance::value_type fromold = from.cbal;
    Balance::value_type   toold = to.cbal; 
    fromold -= amt;
    bool b = unsafe();
    toold += amt;
    if(!b){         
        from.cbal = fromold;
        to.cbal = toold;
    }else{
        success = true;
    }
    return success;
}

// Locked, atomic, not (yet) Except-safe. It will be in C++17
// 2 lines extra code to deal with overhead.
void transfer_atomic(Balance& from, Balance& to, Balance::value_type amt){
    __transaction_atomic{
        from.cbal -= amt;
        if(unsafe())
            __transaction_cancel;
        to.cbal += amt;
    };
    // And what if some does {Balance b; b.lock(), b.cbal=0; b.unlock()}? << death by race.
    // So now we need to protect the entire interface of Balance.
}




/**
 * Interface is protected with atomic blocks, else any new user of this struct exposes races.
 */
struct AtomicBalance{
public:
    typedef double value_type;
private:
    value_type cbal;
public:
    constexpr AtomicBalance(value_type d):cbal(d){;}
    
    constexpr AtomicBalance(const AtomicBalance& rhs):cbal(rhs.cbal){;}
    
    
    AtomicBalance& operator+=(const AtomicBalance& r){
        __transaction_atomic{
            this->cbal += r.cbal;
            return *this;
        };
    }
    
    AtomicBalance& operator-=(const AtomicBalance& r){
        __transaction_atomic{
            this->cbal -= r.cbal;
            return *this;
        };
    }
    
    value_type getBalance()const{
        __transaction_atomic{
                return cbal;
        };
    }
    
    [[transaction_unsafe]]
    void writeBalance()const{std::cout << this->cbal << std::endl;}
};

/**
 * If the transaction fails (cancel), retry until it is commits.
 * Equivalent of a blocking CAS loop with rollbacks.
 */
void transfer_atomic_nested_cas(AtomicBalance& lhs, AtomicBalance& rhs, AtomicBalance::value_type amt){
    AtomicBalance boxed(amt);  
    bool work_done = false;
    for(int i  = 0; i<2; ++i){
        __transaction_atomic{
            work_done = true;       
            lhs -= boxed;       
            if(i==0){   // if (unsafe())
                __transaction_cancel; 
            }
            rhs += boxed;
            if(work_done)
                return;
        };
    }
}

void playBank(AtomicBalance& a, AtomicBalance& b){
        transfer_atomic_nested_cas(a, b, 20);
}

void playCustomer(AtomicBalance& a, AtomicBalance& b){
        transfer_atomic_nested_cas(a, b, 25);
}

int main(int argc, char** argv) {
    AtomicBalance push(200);
    AtomicBalance pull(200);
    
    std::vector<std::thread> threads;
    for(int j = 0; j<std::thread::hardware_concurrency();  ++j){
        if(j % 2){
            threads.push_back( std::thread( playBank, std::ref(push), std::ref(pull)) );
        }
        else{
            threads.push_back(std::thread( playCustomer, std::ref(pull), std::ref(push)) );
        }
    }
    
    for (auto& t : threads){
        t.join();
    }

    std::cout << pull.getBalance() << std::endl;
    assert(pull.getBalance() == 190);
    std::cout << push.getBalance() << std::endl;
    assert(push.getBalance() == 210);
    return 0;
}

