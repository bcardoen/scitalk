# README #


### A (pre)view of transcactional memory in C++17 ###

#### Requirements

* G++ 6.*
* libitm

#### Compilation

* g++ -std=c++1z -fgnu-tm
* Use the makefile provided